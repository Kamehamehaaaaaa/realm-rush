﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Enemy))]
public class EnemyHealth : MonoBehaviour
{
    [SerializeField] int maxHitPoints = 5;
    
    [Tooltip("Adds amount to maxHitPoints when enemy dies.")]
    [SerializeField] int difficultyRamp = 1;

    [SerializeField] AudioClip enemyDeathSFX;
    
    int currentHitPoints = 0;
    AudioSource audioSource;

    Enemy enemy;

    void OnEnable()
    {
        currentHitPoints = maxHitPoints;
    }

    void Start() 
    {
        enemy = GetComponent<Enemy>();
        audioSource = FindObjectOfType<AudioSource>();
    }

    void OnParticleCollision(GameObject other)
    {
        ProcessHit();
    }

    void ProcessHit()
    {
        currentHitPoints--;
        
        if (currentHitPoints <= 0)
        {
            gameObject.SetActive(false);
            audioSource.PlayOneShot(enemyDeathSFX);
            maxHitPoints += difficultyRamp;
            enemy.RewardGold();
        }
    }
}
